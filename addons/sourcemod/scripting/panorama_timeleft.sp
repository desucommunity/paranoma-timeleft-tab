#include <sourcemod>
#include <clientprefs>
#include <multicolors>

Handle timeleft_cookie;
bool timeleft_ativo[MAXPLAYERS + 1];

public Plugin myinfo = 
{
	name = "Panorama - Timeleft",
	author = "Fastmancz",
	description = "Shows Timeleft at the bottom of the screen",
	version = "1.0.0"
};

public void OnPluginStart()
{
	timeleft_cookie = RegClientCookie("timeleft_cookie", "Timeleft Cookie", CookieAccess_Private);	
	RegConsoleCmd("sm_timeleft", Command_Timeleft);
	CreateTimer(1.0, Timeleft, _, TIMER_REPEAT);
}

public Action Command_Timeleft(int client, int args)
{
	if(timeleft_ativo[client])
	{
		timeleft_ativo[client] = false;
		SetClientCookie(client, timeleft_cookie, "0");
		CPrintToChat(client,"[{orange}Desu{default}]  {darkred}Desativaste{default} o tempo restante.")
	}
	else
	{
		timeleft_ativo[client] = true;
		SetClientCookie(client, timeleft_cookie, "1");
		CPrintToChat(client,"[{orange}Desu{default}] {darkred}Ativaste{default} o tempo restante.");
	}
}

public void OnClientCookiesCached(int client)
{
	if(!IsValidClient(client) && IsFakeClient(client))
		return;
		
	char scookie[8];
	GetClientCookie(client, timeleft_cookie, scookie, sizeof(scookie));
	
	// Player already selected timeleft option
	if(!StrEqual(scookie, ""))
	{
		timeleft_ativo[client] = !!StringToInt(scookie);
	}
	else
	{
		timeleft_ativo[client] = true;
		SetClientCookie(client, timeleft_cookie, "1");
	}
}

public Action Timeleft(Handle timer)
{
	char time[60];
	int iTimeleft;

	GetMapTimeLeft(iTimeleft);
	//Format(time, sizeof(time), "%i:%i", iTimeleft / 60,iTimeleft % 60);
	//FormatTime(time, sizeof(time), "%M:%S", iTimeleft);
	if(iTimeleft > 0) FormatEx(time, sizeof(time), "%d:%02d", iTimeleft / 60, iTimeleft % 60);
	else time = "Last Round";

	for(int i = 1; i <= MaxClients; i++)
	{
		if(IsValidClient(i) && !IsFakeClient(i) && IsPlayerAlive(i) && ( GetClientButtons(i) & IN_SCORE ) && timeleft_ativo[i])
		{
			char message[60];
			Format(message, sizeof(message), "Timeleft: %s", time);
			SetHudTextParams(-1.0, 1.00, 1.0, 4, 180, 255, 255, 0, 0.00, 0.00, 0.00);
			ShowHudText(i, -1, message);
		}
	}
	return Plugin_Continue;
}
public bool IsValidClient(int client)
{
	return client > 0 && client <= MaxClients && IsClientInGame(client);
}